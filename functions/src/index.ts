import * as functions from "firebase-functions";

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
export const api = functions
  .region("europe-west2")
  .https.onRequest((request, response) => {
    response.send("Api response :-)");
  });
